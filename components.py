from sdl2 import SDL_QueryTexture, SDL_DestroyTexture
from sdl2.ext import Resources
from ctypes import c_int
import random

screen_size = (960, 540)

RESOURCES = Resources("resources")

"""
Components go here. These contain all of the raw data, but NO logic beyond initialization stuff.
"""


class Brain(object):
    """
    A Brain component for Enemies. This should contain some data that the AISystem uses
    to guide their actions. It could be a simple attack pattern, etc.
    """
    def __init__(self, pattern):
        super().__init__()
        self.intelligence = 0
        self.in_action = True
        self.cooldown = 500
        self.attack_pattern = pattern

        if self.attack_pattern == "bezier_path":
            n = random.randint(4, 10)
            self.coorArrX = []
            self.coorArrY = []
            self.num_control_points = n
            self.locked_looking_down = random.randint(0, 1)
            for k in range(n):
                x = random.randint(0, screen_size[0] - 50)
                y = random.randint(0, screen_size[1] - 60)
                self.coorArrX.append(x)
                self.coorArrY.append(y)
            self.current_step = 0
            self.max_steps = 200

        elif self.attack_pattern == "divebomb":
            self.divebomb_phase = 0
            self.divebomb_ticks = 0
            self.divebomb_wrapped = False
            self.divebomb_right = False
            if random.randint(0,1) < 1:
                self.divebomb_right = True

        elif self.attack_pattern == "ufo":
            self.ticks_before_change = random.randint(100, 200)
            self.current_ticks = self.ticks_before_change - 1
            self.current_movement_choice = random.randint(0, 10)

        elif self.attack_pattern == "invade":
            self.invade_direction = random.randint(0, 1)


class Animation(object):
    """
    This is a list of frames on a Sprite's spritesheet. These should be provided as
    a list of tuples, of (x, y, w, h) for each animation frame. Different animation
    lists for other entities must inherit from the original Animation component in
    order to be picked up by the Entity System.
    """
    def __init__(self, w=32, h=32, fps=15):
        super().__init__()
        pass


class PlayerAnimation(Animation):
    def __init__(self, w, h, fps):
        super().__init__()
        self.w = w
        self.h = h
        self.index = 0
        self.frame_time = 1000 / fps
        self.elapsed_ticks = 0
        self.idle = [(64, 0, self.w, self.h), (64, 48, self.w, self.h)]
        self.mid_left = [(32, 0, self.w, self.h), (32, 48, self.w, self.h)]
        self.far_left = [(0, 0, self.w, self.h), (0, 48, self.w, self.h)]
        self.mid_right = [(96, 0, self.w, self.h), (96, 48, self.w, self.h)]
        self.far_right = [(128, 0, self.w, self.h), (128, 48, self.w, self.h)]


class CursorAnimation(Animation):
    def __init__(self, w, h, fps):
        super().__init__()
        self.w = w
        self.h = h
        self.index = 0
        self.frame_time = 1000 / fps
        self.elapsed_ticks = 0
        self.idle = [(0, 0, self.w, self.h), (self.w, 0, self.w, self.h)]


class PowerupAnimation(Animation):
    def __init__(self, w, h, fps):
        super().__init__()
        self.w = w
        self.h = h
        self.index = 0
        self.frame_time = 1000 / fps
        self.elapsed_ticks = 0
        self.idle = [(0, 0, self.w, self.h), (self.w, 0, self.w, self.h)]
        self.mid_left = self.idle
        self.far_left = self.idle
        self.mid_right = self.idle
        self.far_right = self.idle


class ExplosionAnimation(Animation):
    def __init__(self, w, h, fps):
        super().__init__()
        self.w = w
        self.h = h
        self.index = 0
        self.frame_time = 1000 / fps
        self.elapsed_ticks = 0
        self.idle = [(0, 0, self.w, self.h), (32, 0, self.w, self.h), (64, 0, self.w, self.h),
                     (96, 0, self.w, self.h), (128, 0, self.w, self.h), (160, 0, self.w, self.h),
                     (192, 0, self.w, self.h), (224, 0, self.w, self.h)]
        self.mid_left = self.idle
        self.far_left = self.idle
        self.mid_right = self.idle
        self.far_right = self.idle


class EnemyAnimation(Animation):
    def __init__(self, w, h, fps):
        super().__init__()
        self.w = w
        self.h = h
        self.index = 0
        self.frame_time = 1000 / fps
        self.elapsed_ticks = 0
        self.idle = [(0, 0, self.w, self.h), (self.w, 0, self.w, self.h)]
        self.mid_left = self.idle
        self.far_left = self.idle
        self.mid_right = self.idle
        self.far_right = self.idle


class Commands(object):
    """
    Commands component to hold command state. For the player, these may be set directly via
    input. For Enemies, these should be set by the EnemyMovementSystem (or appropriate AI System).
    """
    def __init__(self):
        super().__init__()
        self.move_left = False
        self.move_right = False
        self.move_up = False
        self.move_down = False
        self.jump_left = False
        self.jump_right = False
        self.jump_up = False
        self.jump_down = False
        self.rotate_left = False
        self.rotate_right = False
        self.attack = False
        self.enemy_attack = False


class Velocity(object):
    """
    A Component to hold speed, accel/decel and max velocity state.
    """
    def __init__(self, maximum=2, accel=0.2, decel=0.8, x=0, y=0, angular=0):
        super().__init__()
        self.x = x
        self.y = y
        self.max = maximum
        self.accel = accel
        self.decel = decel
        self.angular = angular


class Sprite(object):
    """
    A Sprite object. This has simple x, y coordinates, width and height, as well as depth
    (for rendering sort order) and optional angle (rotation) state. The @property and @.setter
    decorators are for convenience, as you often need to get "area" for example in other
    parts of the game engine. The src_rect is an (x, y, w, h) that represents where to copy from
    on the spritesheet (the texture). If the sprite is NOT animated, it will remain as-is (full texture).
    """
    def __init__(self, texture,  x=0, y=0, w=32, h=32, depth=0):
        super().__init__()
        self.texture = texture
        self.x = x
        self.y = y
        self.w = w
        self.h = h
        self.depth = depth
        self.angle = 0
        self.zoom = 0
        self.src_rect = (0, 0, w, h)        # x, y, w, h

    @property
    def size(self):
        """The size of the Sprite as a tuple."""
        return self.w, self.h

    @property
    def texture_size(self):
        """The full size of the Sprites's texture (possibly a spritesheet) as a tuple."""
        w = c_int()
        h = c_int()
        SDL_QueryTexture(self.texture, None, None, w, h)
        return w.value, h.value

    @property
    def position(self):
        """Return the top-left position of the Sprite as tuple."""
        return self.x, self.y

    @position.setter
    def position(self, value):
        """ Set the top-left position of the Sprite. """
        self.x, self.y = value

    @property
    def area(self):
        """ The Sprite's Axis-Aligned-Bounding-Box (rect)."""
        return self.x, self.y, self.x + self.w, self.y + self.h

    @property
    def mid_x(self):
        """ :return: The center of the sprite on the x axis """
        return self.x + self.w / 2

    @property
    def mid_y(self):
        """ :return: The center of the sprite on the y axis """
        return self.y + self.h / 2

    @property
    def mid_position(self):
        return self.x + self.w / 2, self.y + self.h / 2

    def __del__(self):
        """ Clear the SDL texture when deleting the Sprite."""
        if self.texture is not None:
            SDL_DestroyTexture(self.texture)
        self.texture = None


class SFX(object):
    """
    A sound effects Component. Player and different enemies should inherit from the main SFX
    Component, and add their own unique sounds. All Components should have the same
    attack, damange, death and play_me attributes.
    """
    def __init__(self):
        super().__init__()
        self.attack = None
        self.damage = None
        self.death = None
        self.play_me = None


class PlayerSFX(SFX):
    def __init__(self):
        super().__init__()
        self.attack = "jfxr_player_shoot.wav"
        self.damage = "jfxr_explosion_1.wav"
        self.death = "jfxr_explosion_2.wav"
        self.play_me = None


class EnemySFX(SFX):
    def __init__(self):
        super().__init__()
        self.attack = "jfxr_enemy_shoot.wav"
        self.damage = "jfxr_explosion_1.wav"
        self.death = "jfxr_explosion_3.wav"
        self.play_me = None


class Body(object):
    """
    A Component for any Entities that are collidable. A list of one or more (x, y, w, h)
    tuples should be supplied for calculating collisions. Entity type should also be set.
    """
    def __init__(self, rects, player=False, enemy=False, bullet=False, powerup=False):
        super().__init__()
        self.rects = rects
        self.player = player
        self.enemy = enemy
        self.bullet = bullet
        self.powerup = powerup
        self.has_collided = False


class Life(object):
    """ A Component for Entities that can "die", or have a finite lifespan. """
    def __init__(self, lifespan=None, parent_id=None):
        super().__init__()
        self.is_alive = True
        self.lifespan = lifespan
        self.parent_id = parent_id
        self.extra_lives = 3


class TextString(object):
    """ A Component for any Entity that renders text.  """
    def __init__(self, text, color):
        super().__init__()
        self.text = text
        self.previous = ""
        self.color = color


class PlayerStats(object):
    """ A Component specifically for tracking the Player's statistics """
    def __init__(self):
        super().__init__()
        self.current_score = 0
        self.prev_score = 0
        self.extra_lives = 3


class Zoom(object):
    """
    A Component for scaling visible Entities in or out. The ZoomSystem is very simplistic,
    so the zoom values should be supplied in +/- number of pixels (not in percentage).
    """
    def __init__(self, zoom_target=0, zoom_speed=4):
        super().__init__()
        self.zoom_target = zoom_target
        self.zoom_speed = zoom_speed


class Emitter(object):
    """ A Component for tagging Entities which should create particles. """
    def __init__(self, color, size=2, max_life=10):
        super().__init__()
        self.color = color
        self.size = size
        self.max_life = max_life


class Ident(object):
    """To keeping track of which enemies to respawn if the player dies. """
    def __init__(self, num):
        super().__init__()
        self.num = num
