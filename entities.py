import sdl2.ext as ext
from components import *

screen_size = (960, 540)


"""
Entity templates should go here:
"""


class Player(ext.Entity):
    def __init__(self, world, sprite, posx=0, posy=0):
        self.sprite = sprite
        self.sprite.position = (posx, posy)
        self.sprite.src_rect = 0, 0, self.sprite.w, self.sprite.h
        self.velocity = Velocity(maximum=5, decel=0.96)
        self.commands = Commands()
        self.animation = PlayerAnimation(w=self.sprite.w, h=self.sprite.h, fps=30)
        self.sfx = PlayerSFX()
        self.body = Body(rects=self.sprite.area, player=True)
        self.life = Life()


class Enemy(ext.Entity):
    def __init__(self, world, sprite, pattern, ident, posx=0, posy=0, speed=3.0):
        self.ident = Ident(num=ident)
        self.sprite = sprite
        self.sprite.position = (posx, posy)
        self.sprite.src_rect = 0, 0, self.sprite.w, self.sprite.h
        self.velocity = Velocity(maximum=speed)
        self.commands = Commands()
        self.animation = EnemyAnimation(w=self.sprite.w, h=self.sprite.h, fps=30)
        self.brain = Brain(pattern=pattern)
        self.sfx = EnemySFX()
        self.body = Body(rects=self.sprite.area, enemy=True)
        self.life = Life(parent_id=self.id)
        self.zoom = Zoom()


class Bullet(ext.Entity):
    def __init__(self, world, sprite, posx, posy, velx, vely, lifespan):
        self.sprite = sprite
        self.sprite.position = (posx, posy)
        self.sprite.src_rect = 0, 0, self.sprite.w, self.sprite.h
        self.velocity = Velocity(decel=1, x=velx, y=vely)      # decel=1 means no decel.
        self.commands = Commands()
        self.sfx = SFX()
        self.body = Body(rects=self.sprite.area, bullet=True)
        self.life = Life(lifespan=lifespan, parent_id=self.id)
        self.emitter = Emitter(color=(175, 55, 55))


class Explosion(ext.Entity):
    def __init__(self, world, sprite, posx, posy, lifespan):
        self.sprite = sprite
        self.sprite.position = (posx, posy)
        self.sprite.src_rect = 0, 0, self.sprite.w, self.sprite.h
        self.animation = ExplosionAnimation(w=32, h=32, fps=15)
        self.velocity = Velocity()
        self.sfx = SFX()
        self.sfx.play_me = "jfxr_explosion_2.wav"
        self.body = Body(rects=[])
        self.life = Life(lifespan=lifespan, parent_id=self.id)


class Powerup(ext.Entity):
    def __init__(self, world, sprite, posx, posy):
        self.sprite = sprite
        self.sprite.position = (posx, posy)
        self.sprite.src_rect = 0, 0, self.sprite.w, self.sprite.h
        self.animation = PowerupAnimation(w=self.sprite.w, h=self.sprite.h, fps=15)
        self.velocity = Velocity()
        self.body = Body(rects=[self.sprite.area])


class StaticGraphic(ext.Entity):
    def __init__(self, world, sprite, depth, posx=0, posy=0):
        self.sprite = sprite
        self.sprite.position = (posx, posy)
        self.sprite.src_rect = 0, 0, self.sprite.w, self.sprite.h
        self.sprite.depth = depth
        self.zoom = Zoom()


class TextGraphic(ext.Entity):
    def __init__(self, world, sprite, depth, posx, posy, text, zoom=0, color=(255, 255, 255)):
        self.sprite = sprite
        self.sprite.depth = depth
        self.sprite.position = (posx, posy)
        self.sprite.src_rect = 0, 0, self.sprite.w, self.sprite.h
        self.textstring = TextString(text=text, color=color)
        self.zoom = Zoom(zoom_target=zoom)


class AnimatedCursor(ext.Entity):
    def __init__(self, world, sprite, depth, posx, posy):
        self.sprite = sprite
        self.sprite.depth = depth
        self.sprite.position = (posx, posy)
        self.sprite.src_rect = 0, 0, self.sprite.w, self.sprite.h
        self.animation = CursorAnimation(w=self.sprite.w, h=self.sprite.h, fps=30)
        self.velocity = Velocity()


class Hud(ext.Entity):
    def __init__(self, world, sprite, depth, posx, posy, text, color=(255, 255, 255)):
        self.sprite = sprite
        self.sprite.depth = depth
        self.sprite.position = (posx, posy)
        self.sprite.src_rect = 0, 0, self.sprite.w, self.sprite.h
        self.textstring = TextString(text=text, color=color)
        self.playerstats = PlayerStats()


class Particle(ext.Entity):
    def __init__(self, world, sprite, pos, lifespan):
        self.sprite = sprite
        self.sprite.position = pos
        self.velocity = Velocity(decel=1)       # no decel
        self.life = Life(lifespan=lifespan, parent_id=self.id)
        self.body = Body(rects=[])
