"""
This is just a simple lab for trying out things in a environment similar
to the main game.
"""
import os
os.environ["PYSDL2_DLL_PATH"] = os.path.join(os.getcwd(), "libs")
from sdl2 import *
import sdl2.ext as ext
from utilities import input, sprite_maker
from systems import *
from components import *
from random import randint, uniform


RESOURCES = ext.Resources(path="resources")
TARGETFPS = 60
target_frame_ms = (1000.0 / TARGETFPS) + 0.0000001
screen_size = (960, 540)

old_range = (-1, 1)
new_range = (0, screen_size[1])

def scale_range(old_range, new_range, number):
    return (number - old_range[0]) * (new_range[1] - old_range[0]) / (old_range[1] - old_range[0]) + new_range[0]

def draw_points(renderer, gen):
    last_points = (0, 0)
    for column in range(screen_size[0]):
        row = int(scale_range(old_range, new_range, next(gen)))
        renderer.draw_line(last_points + (column, row))
        last_points = (column, row)
        renderer.present()


"""
A simple particle entity:
"""
class Particle(ext.Entity):
    def __init__(self, world, sprite, pos, lifespan):
        self.sprite = sprite
        self.sprite.position = pos
        self.velocity = Velocity(decel=1)       # no decel
        self.life = Life(lifespan=lifespan, parent_id=self.id)
        self.body = Body(rects=[])



def run():
    # Create a new window and renderer.
    window_flags = SDL_RENDERER_PRESENTVSYNC | SDL_WINDOW_RESIZABLE
    window_flags = None
    window = ext.Window(title="Pylaxian", size=screen_size, flags=window_flags)
    window.show()
    renderer = ext.Renderer(target=window)
    SDL_RenderSetLogicalSize(renderer.renderer, screen_size[0], screen_size[1])
    controller = input.Input()

    render_sys = RenderSystem(renderer=renderer)
    life_sys = LifeSystem(renderer=renderer)
    movement_sys = MovementSystem(minx=0, miny=0, maxx=screen_size[0], maxy=screen_size[1])
    world = ext.World()
    world.add_system(render_sys)
    world.add_system(life_sys)
    world.add_system(movement_sys)

    mouse_pos = (0, 0)

    def _create_particle(pos, color, velx, vely, lifespan):
        size = randint(2, 4)
        particle = Particle(world=world,
                            sprite=sprite_maker.from_color(renderer=renderer, color=color, size=(size, size)),
                            pos=pos,
                            lifespan=lifespan)
        particle.velocity.x, particle.velocity.y = velx, vely

    def _plot_point(point):
        _create_particle(pos=point, color=(244, 22, 22), velx=uniform(-0.1, 0.1), vely=uniform(-0.1, 0.1),
                         lifespan=randint(5, 10))

    column = 0

    running = True
    while running:
        start_time = SDL_GetTicks()
        new_events = ext.get_events()
        controller.update(new_events)
        for event in new_events:
            if event.type == SDL_QUIT:
                running = False
            elif event.type == SDL_MOUSEMOTION:
                mouse_pos = event.motion.x, event.motion.y
        if controller.current_inputs["back"]:
            running = False

        mouse_pos = (screen_size[0]/2, screen_size[1]/2)

        _create_particle(pos=mouse_pos, velx=uniform(-2, 2), vely=uniform(-2, 2),
                         color=(randint(0, 255), randint(0, 255), randint(0, 255)), lifespan=200)

        """
        _create_particle(pos=(mouse_pos[0] - 100, mouse_pos[1]), velx=uniform(-2, 2), vely=uniform(-2, 2),
                         color=(randint(55, 255), randint(0, 55), randint(0, 55)), lifespan=randint(100, 200))

        _create_particle(pos=mouse_pos, velx=uniform(-2, 2), vely=uniform(-2, 2),
                         color=(randint(0, 55), randint(55, 255), randint(0, 55)), lifespan=randint(100, 200))

        _create_particle(pos=(mouse_pos[0] + 100, mouse_pos[1]), velx=uniform(-2, 2), vely=uniform(-2, 2),
                         color=(randint(0, 55), randint(0, 55), randint(55, 255)), lifespan=randint(100, 200))
        """

        world.process()

        """
        # This limiter is crude, but should be sufficient to freeze Captain So....  wait.
        current_time = SDL_GetTicks()
        sleep_time = int(start_time + target_frame_ms - current_time)
        if sleep_time > 0:
            SDL_Delay(sleep_time)
        """

    ext.quit()

if __name__ == "__main__":
    run()
    exit()
