from utilities.scene import Scene
from systems import *
from sdl2.sdlmixer import Mix_LoadMUS, Mix_VolumeMusic, Mix_Volume
from entities import *
from sdl2 import SDL_SetWindowFullscreen, SDL_WINDOW_FULLSCREEN_DESKTOP, SDL_WINDOW_RESIZABLE
from random import randint, uniform

"""
Scene definitions should go here:
"""


class SplashScreen(Scene):
    def __init__(self, renderer, controller):
        super().__init__()
        self.scene_name = "splash_screen"
        self.renderer = renderer
        self.controller = controller
        self.clear_color = [0, 0, 0]
        self.fade_in = True
        self.fade_out = False
        self.music = None
        self.world = ext.World()

        render_sys = RenderSystem(renderer=self.renderer)
        life_sys = LifeSystem(renderer=renderer)
        movement_sys = MovementSystem(minx=-10, miny=-10, maxx=screen_size[0] + 10, maxy=screen_size[1] + 10)
        text_sys = TextSystem(renderer=self.renderer, font="manaspc.ttf", dpi=44)
        self.world.add_system(render_sys)
        self.world.add_system(life_sys)
        self.world.add_system(movement_sys)
        self.world.add_system(text_sys)

        intro_sprite_b1 = sprite_maker.from_color(renderer=renderer, color=(0, 0, 0), size=(1, 1))
        intro_sprite_f1 = sprite_maker.from_color(renderer=renderer, color=(0, 0, 0), size=(1, 1))
        intro_sprite_b2 = sprite_maker.from_color(renderer=renderer, color=(0, 0, 0), size=(1, 1))
        intro_sprite_f2 = sprite_maker.from_color(renderer=renderer, color=(0, 0, 0), size=(1, 1))
        intro_bg = TextGraphic(world=self.world, sprite=intro_sprite_b1, depth=2, posx=screen_size[0] / 3,
                               posy=screen_size[1] / 3, text="A game by:", color=(250, 250, 250))
        intro_fg = TextGraphic(world=self.world, sprite=intro_sprite_f1, depth=1, posx=screen_size[0] / 3 + 4,
                               posy=screen_size[1] / 3 + 4, text="A game by:", color=(0, 0, 0))
        intro_bg2 = TextGraphic(world=self.world, sprite=intro_sprite_b2, depth=2, posx=65,
                                posy=screen_size[1] / 2, text="Benjamin Moran & John Babich", color=(250, 250, 250))
        intro_fg2 = TextGraphic(world=self.world, sprite=intro_sprite_f2, depth=1, posx=65 + 4,
                                posy=screen_size[1] / 2 + 4, text="Benjamin Moran & John Babich", color=(0, 0, 0))

    def _create_particle(self, pos, velx, vely, lifespan):
        size = randint(2, 6)
        color = randint(0, 255), randint(0, 255), randint(0, 255)
        particle = Particle(world=self.world,
                            sprite=sprite_maker.from_color(renderer=self.renderer, color=color, size=(size, size)),
                            pos=pos,
                            lifespan=lifespan)
        particle.velocity.x, particle.velocity.y = velx, vely

    def update_world(self):
        self._create_particle(pos=(screen_size[0] / 2, screen_size[1] / 2), velx=uniform(-7, 7), vely=uniform(-4, 4),
                              lifespan=randint(100, 200))
        if self.fade_in:
            self.clear_color[:] = [member + 3 for member in self.clear_color]
            if self.clear_color[0] >= 250:
                self.fade_in = False
                self.fade_out = True
        elif self.fade_out:
            self.clear_color[:] = [member - 4 for member in self.clear_color]
            if self.clear_color[0] <= 2:
                self.fade_out = False
        else:
            self.next_scene = "title_screen"
        self.renderer.color = self.clear_color
        self.world.process()


class TitleScreen(Scene):
    def __init__(self, renderer, controller, config):
        super().__init__()
        self.scene_name = "title_screen"
        self.world = ext.World()
        self.cursor_positions = [(325, 325), (325, 375), (325, 425)]
        self.current_pos = 0
        self.renderer = renderer
        self.controller = controller
        self.config = config
        self.music = Mix_LoadMUS(RESOURCES.get_path("day_50.ogg").encode())

        background_sprite = sprite_maker.from_image(renderer, RESOURCES.get_path("titlescreen.png"))
        cursor_sprite = sprite_maker.from_image(renderer, RESOURCES.get_path("enemy-three.png"), (16, 16))
        start_sprite = sprite_maker.from_color(renderer=renderer, color=(255, 255, 255), size=(0, 0))
        options_sprite = sprite_maker.from_color(renderer=renderer, color=(255, 255, 255), size=(0, 0))
        exit_sprite = sprite_maker.from_color(renderer=renderer, color=(255, 255, 255), size=(0, 0))
        highscore_sprite = sprite_maker.from_color(renderer=renderer, color=(255, 255, 255), size=(0, 0))
        score1_sprite = sprite_maker.from_color(renderer=renderer, color=(255, 255, 255), size=(0, 0))
        score2_sprite = sprite_maker.from_color(renderer=renderer, color=(255, 255, 255), size=(0, 0))
        score3_sprite = sprite_maker.from_color(renderer=renderer, color=(255, 255, 255), size=(0, 0))
        self.title = StaticGraphic(world=self.world, sprite=background_sprite, depth=-1)
        self.title.sprite.zoom = -250
        self.title.zoom.zoom_speed = 6
        self.cursor = AnimatedCursor(world=self.world, sprite=cursor_sprite, depth=1, posx=325, posy=325, )
        self.start_text = TextGraphic(world=self.world, sprite=start_sprite, depth=1, posx=375, posy=320,
                                      text="Start / Continue")
        self.options_text = TextGraphic(world=self.world, sprite=options_sprite, depth=1, posx=375, posy=370,
                                        text="Options")
        self.exit_text = TextGraphic(world=self.world, sprite=exit_sprite, depth=1, posx=375, posy=420,
                                     text="Exit Game")
        self.highscore_text = TextGraphic(world=self.world, sprite=highscore_sprite, depth=1, posx=5,
                                          posy=screen_size[1] - 140, text="Top Three:")
        self.score1 = TextGraphic(world=self.world, sprite=score1_sprite, depth=1, posx=20,
                                  posy=screen_size[1] - 105, text="", color=(75, 75, 255))
        self.score2 = TextGraphic(world=self.world, sprite=score2_sprite, depth=1, posx=20,
                                  posy=screen_size[1] - 70, text="", color=(50, 50, 175))
        self.score3 = TextGraphic(world=self.world, sprite=score3_sprite, depth=1, posx=20,
                                  posy=screen_size[1] - 35, text="", color=(20, 20, 100))

        text_sys = TextSystem(renderer=self.renderer, font="lunchds.ttf", dpi=31)
        animation_sys = AnimationSystem()
        render_sys = RenderSystem(renderer=self.renderer)
        zoom_sys = ZoomSystem()
        self.world.add_system(text_sys)
        self.world.add_system(render_sys)
        self.world.add_system(animation_sys)
        self.world.add_system(zoom_sys)

    def update_world(self):
        if self.controller.current_inputs["start"] and not self.controller.prev_inputs["start"] or \
                        self.controller.current_inputs["a"] and not self.controller.prev_inputs["a"]:
            self.title.sprite.zoom = 0
            if self.current_pos == 0:
                self.next_scene = "ready_player_one"
            elif self.current_pos == 1:
                self.next_scene = "options"
            elif self.current_pos == 2:
                print("Exiting...")
                self.controller.current_inputs["back"] = True
        if self.controller.current_inputs["up"] and not self.controller.prev_inputs["up"]:
            if self.current_pos > 0:
                self.current_pos -= 1
                self.controller.rumble(5, 70)
        if self.controller.current_inputs["down"] and not self.controller.prev_inputs["down"]:
            if self.current_pos < len(self.cursor_positions) - 1:
                self.current_pos += 1
                self.controller.rumble(5, 70)

        # Move the cursor's sprite to patch the position:
        self.cursor.sprite.position = self.cursor_positions[self.current_pos]
        self.cursor.sprite.angle += 2

        # Hack to make the current selected menu item zoom in.
        # TODO: keep track of menu position in a sane manner, and get rid of these hacks.
        self.start_text.sprite.zoom, self.options_text.sprite.zoom, self.exit_text.sprite.zoom = -2, -2, -2
        if self.current_pos == 0:
            self.start_text.sprite.zoom = 6
        elif self.current_pos == 1:
            self.options_text.sprite.zoom = 6
        elif self.current_pos == 2:
            self.exit_text.sprite.zoom = 6

        # Updating the highscore text every frame is wasteful, but it's only the title screen.
        self.score1.textstring.text = "1: " + str(self.config.dat["highscore1"])
        self.score2.textstring.text = "2: " + str(self.config.dat["highscore2"])
        self.score3.textstring.text = "3: " + str(self.config.dat["highscore3"])

        self.world.process()


class LevelOne(Scene):
    def __init__(self, renderer, controller, config):
        super().__init__()
        self.scene_name = "level_one"
        self.world = ext.World()
        self.renderer = renderer
        self.controller = controller
        self.config = config
        self.autofire_limit = 10
        self.autofire_ticker = 0
        self.music = Mix_LoadMUS(RESOURCES.get_path("day_47.ogg").encode())

        player_sprite = sprite_maker.from_image(renderer, RESOURCES.get_path("ship.png"), (32, 48), depth=1)
        self.player = Player(world=self.world, sprite=player_sprite, posx=screen_size[0] / 2 - 16,
                             posy=screen_size[1] - 40)
        hud_sprite = sprite_maker.from_color(renderer=renderer, color=(255, 255, 255), size=(1, 1))
        self.hud = Hud(world=self.world, sprite=hud_sprite, depth=3, posx=5, posy=5, text="SCORE: 0")

        text_sys = TextSystem(renderer=self.renderer, font="lunchds.ttf", dpi=30)
        hud_sys = HudSystem(player=self.player)
        zoom_sys = ZoomSystem()
        render_sys = RenderSystem(renderer=renderer)
        command_sys = CommandSystem(renderer=renderer)
        movement_sys = MovementSystem(minx=0, miny=0, maxx=screen_size[0], maxy=screen_size[1])
        collision_sys = CollisionSystem(hud=self.hud)
        animation_sys = AnimationSystem()
        enemy_movement_sys = EnemyMovementSystem(player=self.player.sprite)
        sfx_sys = SFXSystem()
        life_sys = LifeSystem(renderer=renderer)
        particle_sys = ParticleSystem(renderer=renderer, world=self.world)
        self.entity_manager_sys = EntityManagerSystem(world=self.world, renderer=renderer, tiled_file="waves.json")
        self.world.add_system(self.entity_manager_sys)
        self.world.add_system(text_sys)
        self.world.add_system(hud_sys)
        self.world.add_system(zoom_sys)
        self.world.add_system(particle_sys)
        self.world.add_system(command_sys)
        self.world.add_system(movement_sys)
        self.world.add_system(collision_sys)
        self.world.add_system(animation_sys)
        self.world.add_system(enemy_movement_sys)
        self.world.add_system(sfx_sys)
        self.world.add_system(life_sys)
        self.world.add_system(render_sys)

    def update_world(self):
        if self.controller.current_inputs["start"] and not self.controller.prev_inputs["start"]:
            self.next_scene = "title_screen"
        if self.controller.current_inputs["left"]:
            self.player.commands.move_left = True
        if self.controller.current_inputs["right"]:
            self.player.commands.move_right = True
        if self.controller.current_inputs["up"]:
            self.player.commands.move_up = True
        if self.controller.current_inputs["down"]:
            self.player.commands.move_down = True
        if self.controller.current_inputs["a"] and not self.controller.prev_inputs["a"]:  # C on the keyboard
            self.player.commands.attack = True
        elif self.controller.current_inputs["a"] and self.controller.prev_inputs["a"]:
            if self.autofire_ticker >= self.autofire_limit:
                self.player.commands.attack = True
                self.autofire_ticker = 0
            else:
                self.autofire_ticker += 1

        if self.player.body.has_collided:
            if not self.player.life.is_alive:
                self.config.update_score(self.hud.playerstats.current_score)
                self.__init__(self.renderer, self.controller, self.config)
                self.next_scene = "game_over"
                self.controller.rumble(5, 200)
            else:
                self.player.body.has_collided = False
                self.player.life.extra_lives -= 1
                self.player.sprite.x = screen_size[0] / 2 - 16
                self.player.sprite.y = screen_size[1] - 40
                self.player.velocity.x = 0
                self.player.velocity.y = 0
                self.entity_manager_sys._initialize_enemy_wave()
                self.next_scene = "ready_player_one"

        self.world.process()


class GameOver(Scene):
    def __init__(self, renderer, controller):
        super().__init__()
        self.scene_name = "game_over"
        self.timeout = 120
        self.world = ext.World()
        self.controller = controller
        self.music = None

        background_sprite = sprite_maker.from_image(renderer=renderer, image_name=RESOURCES.get_path("gameover.png"))
        gameover = StaticGraphic(world=self.world, sprite=background_sprite, depth=-3)
        render_sys = RenderSystem(renderer=renderer)
        self.world.add_system(render_sys)

    def update_world(self):
        if self.timeout > 0:
            self.timeout -= 1
        else:
            # TODO: use actual time instead of countdown (in case the framerate is changed later)
            self.timeout = 120
            self.next_scene = "title_screen"
        self.world.process()


class Options(Scene):
    def __init__(self, renderer, controller, config):
        super().__init__()
        self.scene_name = "options"
        self.world = ext.World()
        self.controller = controller
        self.config = config
        self.renderer = renderer
        self.music = None
        self.cursor_positions = [(325, 125), (325, 175), (325, 225), (325, 275), (325, 325)]
        self.current_pos = 0
        self.checkbox = {True: "[X]", False: "[ ]"}

        background_sprite = sprite_maker.from_color(renderer=renderer, color=(33, 33, 33), size=screen_size)
        background = StaticGraphic(world=self.world, sprite=background_sprite, depth=-3)
        cursor_sprite = sprite_maker.from_image(renderer, RESOURCES.get_path("enemy-three.png"), (16, 16))
        self.cursor = AnimatedCursor(world=self.world, sprite=cursor_sprite, depth=1, posx=325, posy=325, )

        fullscreen_sprite = sprite_maker.from_color(renderer=renderer, color=(255, 255, 255), size=(1, 1))
        rumble_sprite = sprite_maker.from_color(renderer=renderer, color=(255, 255, 255), size=(1, 1))
        return_sprite = sprite_maker.from_color(renderer=renderer, color=(255, 255, 255), size=(1, 1))
        music_vol_sprite = sprite_maker.from_color(renderer=renderer, color=(255, 255, 255), size=(1, 1))
        sfx_vol_sprite = sprite_maker.from_color(renderer=renderer, color=(255, 255, 255), size=(1, 1))

        self.fullscreen_text = TextGraphic(world=self.world, sprite=fullscreen_sprite, depth=1, posx=375, posy=125,
                                           text=self.checkbox[self.config.dat["fullscreen"]] + " Full Screen", zoom=-2)
        self.rumble_text = TextGraphic(world=self.world, sprite=rumble_sprite, depth=1, posx=375, posy=175,
                                       text=self.checkbox[self.config.dat["rumble"]] + " Rumble", zoom=-2)
        self.music_vol_text = TextGraphic(world=self.world, sprite=music_vol_sprite, depth=1, posx=375, posy=225,
                                          text=str(self.config.dat["music_vol"]) + "% music volume", zoom=-2)
        self.sfx_vol_text = TextGraphic(world=self.world, sprite=sfx_vol_sprite, depth=1, posx=375, posy=275,
                                        text=str(self.config.dat["sfx_vol"]) + "% sfx volume", zoom=-2)
        self.return_text = TextGraphic(world=self.world, sprite=return_sprite, depth=1, posx=375, posy=325,
                                       text="< Return", zoom=-2)

        text_sys = TextSystem(renderer=renderer, font="lunchds.ttf", dpi=30)
        render_sys = RenderSystem(renderer=renderer)
        animation_sys = AnimationSystem()
        zoom_sys = ZoomSystem()
        self.world.add_system(text_sys)
        self.world.add_system(render_sys)
        self.world.add_system(animation_sys)
        self.world.add_system(zoom_sys)

    def update_world(self):
        if self.controller.current_inputs["a"] and not self.controller.prev_inputs["a"]:
            if self.current_pos == 0:
                self.config.dat["fullscreen"] = not self.config.dat["fullscreen"]
                self.fullscreen_text.textstring.text = self.checkbox[self.config.dat["fullscreen"]] + " Full Screen"
                self.controller.rumble(5, 88)
                if self.config.dat["fullscreen"]:
                    SDL_SetWindowFullscreen(self.renderer.rendertarget, SDL_WINDOW_FULLSCREEN_DESKTOP)
                else:
                    SDL_SetWindowFullscreen(self.renderer.rendertarget, SDL_WINDOW_RESIZABLE)
            elif self.current_pos == 1:
                self.config.dat["rumble"] = not self.config.dat["rumble"]
                self.rumble_text.textstring.text = self.checkbox[self.config.dat["rumble"]] + " Rumble"
                if self.config.dat["rumble"]:
                    self.controller.init_haptic()
                self.controller.rumble(25, 120)
            elif self.current_pos == 4:
                self.next_scene = "title_screen"
        if self.controller.current_inputs["left"] and not self.controller.prev_inputs["left"]:
            if self.current_pos == 2:
                self.config.dat["music_vol"] -= 10
                self.config.dat["music_vol"] = max(0, self.config.dat["music_vol"])
                self.music_vol_text.textstring.text = str(self.config.dat["music_vol"]) + "% music volume"
                # Max volume is == 128, so the extra math converts it to a 0-100% value:
                Mix_VolumeMusic(int(self.config.dat["music_vol"] * MIX_MAX_VOLUME / 100))
            elif self.current_pos == 3:
                self.config.dat["sfx_vol"] -= 10
                self.config.dat["sfx_vol"] = max(0, self.config.dat["sfx_vol"])
                self.sfx_vol_text.textstring.text = str(self.config.dat["sfx_vol"]) + "% sfx volume"
                Mix_Volume(-1, int(self.config.dat["sfx_vol"] * MIX_MAX_VOLUME / 100))
        if self.controller.current_inputs["right"] and not self.controller.prev_inputs["right"]:
            if self.current_pos == 2:
                self.config.dat["music_vol"] += 10
                self.config.dat["music_vol"] = min(100, self.config.dat["music_vol"])
                self.music_vol_text.textstring.text = str(self.config.dat["music_vol"]) + "% music volume"
                Mix_VolumeMusic(
                    int(self.config.dat["music_vol"] * MIX_MAX_VOLUME / 100))  # max vol is 128. convert to %.
            elif self.current_pos == 3:
                self.config.dat["sfx_vol"] += 10
                self.config.dat["sfx_vol"] = min(100, self.config.dat["sfx_vol"])
                self.sfx_vol_text.textstring.text = str(self.config.dat["sfx_vol"]) + "% sfx volume"
                Mix_Volume(-1, int(self.config.dat["sfx_vol"] * MIX_MAX_VOLUME / 100))
        if self.controller.current_inputs["up"] and not self.controller.prev_inputs["up"]:
            if self.current_pos > 0:
                self.current_pos -= 1
                self.controller.rumble(5, 70)
        if self.controller.current_inputs["down"] and not self.controller.prev_inputs["down"]:
            if self.current_pos < len(self.cursor_positions) - 1:
                self.current_pos += 1
                self.controller.rumble(5, 70)
        if self.controller.current_inputs["b"]:
            self.next_scene = "title_screen"

        # Move the cursor's sprite to patch the position:
        self.cursor.sprite.position = self.cursor_positions[self.current_pos]

        # Hack to make the current selected menu item zoom in.
        if self.current_pos == 0:
            self.fullscreen_text.sprite.zoom = 6
        elif self.current_pos == 1:
            self.rumble_text.sprite.zoom = 6
        elif self.current_pos == 2:
            self.music_vol_text.sprite.zoom = 6
        elif self.current_pos == 3:
            self.sfx_vol_text.sprite.zoom = 6
        elif self.current_pos == 4:
            self.return_text.sprite.zoom = 6

        self.world.process()


class ReadyPlayerOne(Scene):
    def __init__(self, renderer):
        super().__init__()
        self.scene_name = "ready_player_one"
        self.timeout = 60
        self.world = ext.World()
        self.music = None

        get_ready_sprite = sprite_maker.from_color(renderer=renderer, color=(255, 255, 255), size=(1, 1))
        self.get_ready_text = TextGraphic(world=self.world, sprite=get_ready_sprite, depth=0,
                                          posx=0, posy=0, text="Ready Player One!!")

        text_sys = TextSystem(renderer=renderer, font="lunchds.ttf", dpi=30)
        render_sys = RenderSystem(renderer=renderer)
        zoom_sys = ZoomSystem()
        self.world.add_system(render_sys)
        self.world.add_system(text_sys)
        self.world.add_system(zoom_sys)
        self.world.process()

        get_ready_sprite.position = int(screen_size[0] / 2 - get_ready_sprite.texture_size[0] / 2), int(
            screen_size[1] / 2 - 50)
        get_ready_sprite.zoom = -28
        self.get_ready_text.zoom.zoom_speed = 1

    def update_world(self):
        if self.timeout > 0:
            self.timeout -= 1
        else:
            self.timeout = 120
            self.next_scene = "level_one"
            self.get_ready_text.sprite.zoom = -28
        self.world.process()
