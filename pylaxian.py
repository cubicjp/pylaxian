import os
# Try to first load the sdl2 dll from the libs directory.
# If it doesn't exist, we will fall back to the the system path.
os.environ["PYSDL2_DLL_PATH"] = os.path.join(os.getcwd(), "libs")
from sdl2 import *
from systems import *
from utilities import input
from utilities.scene import SceneManager
from utilities.config import Config
import levels

# "Resources" is a handy module for organizing assets.
RESOURCES = ext.Resources(path="resources")
TARGETFPS = 60
target_frame_ms = (1000.0 / TARGETFPS) + 0.0000001
screen_size = (960, 540)
# Load configuration from disk.
conf = Config("battery.rom")


def run():
    # Create a new window and renderer:
    window_flags = SDL_WINDOW_RESIZABLE
    if conf.dat["fullscreen"]:
        window_flags = SDL_WINDOW_RESIZABLE | SDL_WINDOW_FULLSCREEN_DESKTOP
    window = ext.Window(title="Pylaxian", position=None, size=screen_size, flags=window_flags)
    renderer = ext.Renderer(target=window)
    # The screen will scale if resized:
    SDL_RenderSetLogicalSize(renderer.renderer, screen_size[0], screen_size[1])
    # Open audio device and set bitrate, format, channels, and buffer
    Mix_OpenAudio(44100, MIX_DEFAULT_FORMAT, 2, 1024)
    Mix_VolumeMusic(int(conf.dat["music_vol"] * MIX_MAX_VOLUME / 100))  # MIX_MAX_VOLUME == 128. Convert from %.
    Mix_Volume(-1, int(conf.dat["sfx_vol"] * MIX_MAX_VOLUME / 100))  # "-1" sets volume for all sfx channels.
    # Initialize input stuff:
    controller = input.Input()
    if conf.dat["rumble"]:
        controller.init_haptic()

    # Instantiate all of the scenes, and add them to the Scene Manager:
    splash_screen = levels.SplashScreen(renderer=renderer, controller=controller)
    title_screen = levels.TitleScreen(renderer=renderer, controller=controller, config=conf)
    level_one = levels.LevelOne(renderer=renderer, controller=controller, config=conf)
    game_over = levels.GameOver(renderer=renderer, controller=controller)
    options = levels.Options(renderer=renderer, controller=controller, config=conf)
    ready_player_one = levels.ReadyPlayerOne(renderer=renderer)
    scene_manager = SceneManager()
    scene_manager.add_scene(ready_player_one)
    scene_manager.add_scene(level_one)
    scene_manager.add_scene(game_over)
    scene_manager.add_scene(options)
    scene_manager.add_scene(title_screen)
    scene_manager.add_scene(splash_screen)
    Mix_FadeInMusic(scene_manager.current_scene.music, -1, 2000)

    running = True
    while running:
        start_time = SDL_GetTicks()
        new_events = ext.get_events()
        for event in new_events:
            if event.type == SDL_QUIT:
                running = False
        controller.update(new_events)
        if controller.current_inputs["back"]:
            running = False

        scene_manager.update()

        # This limiter is crude, but should be sufficient to freeze Captain So....  wait.
        current_time = SDL_GetTicks()
        sleep_time = int(start_time + target_frame_ms - current_time)
        if sleep_time > 0:
            SDL_Delay(int(sleep_time))

    ext.quit()


if __name__ == "__main__":
    run()
    conf.save_data()
    exit()
