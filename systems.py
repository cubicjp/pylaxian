from sdl2 import SDL_Rect, SDL_RenderCopyEx, SDL_FLIP_NONE, SDL_Error, SDL_Color
from sdl2 import SDL_GetTicks, SDL_FreeSurface, SDL_CreateTextureFromSurface
from sdl2.sdlmixer import *
import sdl2.ext as ext
from sdl2.ext.compat import isiterable
from sdl2.sdlttf import *
from components import *
from entities import Particle, Explosion, Bullet, Enemy
from ctypes import c_int
from math import atan2, degrees, cos, sin, radians
from random import randint
from utilities import sprite_maker, tiled_loader
from pprint import pprint


"""
Systems go here. These contain all of the application logic, but NO data themselves.
"""


class HudSystem(ext.Applicator):
    """ Updates the HUD with player lives and score. """
    def __init__(self, player):
        super().__init__()
        self.componenttypes = (PlayerStats, TextString)
        self.player = player

    def process(self, world, componentsets):
        for stats, txt in componentsets:
            txt.text = "LIVES: {}   SCORE: {}".format(self.player.life.extra_lives, stats.current_score)


class TextSystem(ext.Applicator):
    """
    Creates a new texture from whatever text string the TextString component gets updated with.
    To prevent a new texture from being created every frame, the text is checked for changes.
    """
    def __init__(self, renderer, font, dpi=30):
        super().__init__()
        print("Initializing Text System")
        self.componenttypes = (Sprite, TextString)
        self.renderer = renderer
        self.font = font
        self.dpi = dpi
        TTF_Init()
        self.font = TTF_OpenFont(RESOURCES.get_path(self.font).encode("utf-8"), self.dpi)

    def _make_texture(self, text_string, color):
        sdl_color = SDL_Color(r=color[0], g=color[1], b=color[2], a=0)
        font_surface = TTF_RenderUTF8_Blended(self.font, text_string.encode("UTF-8"), sdl_color)
        self.new_texture = SDL_CreateTextureFromSurface(self.renderer.renderer, font_surface)
        SDL_FreeSurface(font_surface)
        del font_surface
        return self.new_texture

    def process(self, world, componentsets):
        """ only update if the text string has changed from the previous frame """
        for spr, txt in componentsets:
            if txt.text == txt.previous:
                continue
            else:
                SDL_DestroyTexture(spr.texture)
                spr.texture = self._make_texture(text_string=txt.text, color=txt.color)
                texture_width = c_int()
                texture_height = c_int()
                SDL_QueryTexture(spr.texture, None, None, texture_width, texture_height)
                spr.src_rect = (0, 0, texture_width.value, texture_height.value)
                txt.previous = txt.text


class SFXSystem(ext.System):
    """
    Check if any Sound Effects are queued to play, play them, and clear the queue.
    """
    def __init__(self):
        super().__init__()
        print("Initializing SFX System.")
        self.componenttypes = (SFX,)      # The ECS expects a tuple, so the comma is necessary.
        self.sound_cache = {}

    def process(self, world, components):
        for sfx in components:
            if sfx.play_me is not None:
                if sfx.play_me in self.sound_cache:
                    Mix_PlayChannel(-1, self.sound_cache[sfx.play_me], 0)
                else:
                    self.sound_cache[sfx.play_me] = Mix_LoadWAV(RESOURCES.get_path(sfx.play_me).encode())
                    Mix_PlayChannel(-1, self.sound_cache[sfx.play_me], 0)
                sfx.play_me = None


class EnemyMovementSystem(ext.Applicator):
    """
    This is to allow movement/pathing for enemies. Contains logic for several AI types.
    """
    def __init__(self, player):
        super().__init__()
        self.componenttypes = Brain, Commands, Sprite, Velocity
        self.player = player

    def _bezier_draw(self, coorArr, i, j, t):
        """
        This draws one step of bezier path
        """
        if j == 0:
            return coorArr[i]
        return self._bezier_draw(coorArr, i, j - 1, t) * (1 - t) + self._bezier_draw(coorArr, i + 1, j - 1, t) * t

    @staticmethod
    def _set_new_points(brain, coorArrX, coorArrY):
        """
        This sets up entirely new points for next section of a bezier path
        TODO: Pass in bounds of X, Y so can guide the direction of path from calling function
        """
        brain.current_step = 0
        newStartingPointX = coorArrX.pop()
        newStartingPointY = coorArrY.pop()
        coorArrX.clear()
        coorArrY.clear()
        coorArrX.append(newStartingPointX)
        coorArrY.append(newStartingPointY)

        for k in range(brain.num_control_points - 1):
            x = random.randint(0, 900)
            y = random.randint(0, 500)
            coorArrX.append(x)
            coorArrY.append(y)

    def process(self, world, componentsets):
        for brain, commands, sprite, vel in componentsets:
            if brain.attack_pattern == "bezier_path":
                t = float(brain.current_step) / float(brain.max_steps)
                new_x = int(self._bezier_draw(brain.coorArrX, 0, brain.num_control_points - 1, t))
                new_y = int(self._bezier_draw(brain.coorArrY, 0, brain.num_control_points - 1, t))
                # Cap movement to not bash into the player
                if new_y > (screen_size[1] - 200):
                    new_y = (screen_size[1] - 200)
                if new_x != sprite.x:
                    if new_x < sprite.x:
                        commands.move_left = True
                    else:
                        commands.move_right = True
                if new_y != sprite.y:
                    if new_y < sprite.y:
                        commands.move_up = True
                    else:
                        commands.move_down = True

                if brain.locked_looking_down == 1:
                    sprite.angle = 0
                else:
                    sprite.angle = degrees(atan2(vel.y, vel.x)) - 90

                if brain.current_step < brain.max_steps:
                    brain.current_step += 1
                elif brain.current_step == brain.max_steps:
                    self._set_new_points(brain, brain.coorArrX, brain.coorArrY)

            elif brain.attack_pattern == "chase":
                if sprite.y > (self.player.y + 10):
                    commands.move_up = True
                elif sprite.y < (self.player.y - 10):
                    commands.move_down = True
                if sprite.x > (self.player.x + 50):
                    commands.move_left = True
                elif sprite.x < (self.player.x - 50):
                    commands.move_right = True
                sprite.angle = degrees(atan2(vel.y, vel.x)) - 90

            elif brain.attack_pattern == "divebomb":
                # Phase 1: Move up and right or left
                # Phase 2: Move down and opposite of phase 1
                # Phase 3: Move straight down and shoot until loop to top

                if brain.divebomb_phase < 3:
                    brain.divebomb_ticks += 1
                    if brain.divebomb_ticks > 120:
                        brain.divebomb_phase += 1
                        brain.divebomb_ticks = 0

                if brain.divebomb_phase == 0:
                    if brain.divebomb_right:
                        commands.move_right = True
                    else:
                        commands.move_left = True
                    commands.move_up = True
                    commands.jump_up = True
                    brain.divebomb_wrapped = False
                    if sprite.y < 1:
                        brain.divebomb_phase += 1
                    elif sprite.x > screen_size[0]:
                        brain.divebomb_phase += 1
                    elif sprite.x < 1:
                        brain.divebomb_phase += 1
                elif brain.divebomb_phase == 1:
                    if brain.divebomb_right:
                        commands.move_right = True
                    else:
                        commands.move_left = True
                    commands.move_down = True
                    commands.jump_down = True
                elif brain.divebomb_phase == 2:
                    if brain.divebomb_right:
                        commands.move_left = True
                    else:
                        commands.move_right = True
                    commands.move_down = True
                    commands.jump_down = True
                elif brain.divebomb_phase == 3:
                    commands.move_down = True
                    commands.jump_down = True
                    if (brain.divebomb_ticks > 3):
                        brain.divebomb_ticks = 0
                        if random.randint(0, 1000) < (7 + brain.intelligence):
                            commands.enemy_attack = True
                    brain.divebomb_ticks += 1

                if brain.divebomb_wrapped and sprite.y >= 60:
                    brain.divebomb_phase = 0

                if brain.divebomb_phase == 3:
                    # Check to see if sprite needs to wrap to top of screen
                    if sprite.y + sprite.h >= screen_size[1]:
                        sprite.y = 0
                        brain.divebomb_wrapped = True
                    elif sprite.y < 0:
                        sprite.y = 0 + sprite.h

            elif brain.attack_pattern == "ufo":
                # Goal is for the UFO to move for a while, then change direction or stop and shoot
                if brain.current_movement_choice < 3:
                    commands.move_right = True
                elif brain.current_movement_choice < 8:
                    commands.move_left = True
                elif brain.current_movement_choice > 8:
                    if random.randint(0, 100) < brain.intelligence and brain.current_ticks < (brain.ticks_before_change / 2):
                        commands.enemy_attack = True

                if brain.current_ticks < brain.ticks_before_change:
                    brain.current_ticks += 1
                else:
                    brain.current_ticks = 0
                    brain.current_movement_choice = random.randint(0, 10)

                # Rotate to the other side of the screen as appropriate
                if sprite.x + sprite.w == screen_size[0]:
                    sprite.x = 0
                elif sprite.x <= 0:
                    sprite.x = screen_size[0] - sprite.w

            elif brain.attack_pattern == "invade":
                if random.randint(0, 100) < 1 + brain.intelligence:
                    commands.enemy_attack = True

                if brain.invade_direction == 0:
                    """
                    Algorithm:
                    1. Move right until hit edge of screen,
                    2. Reduce height and change invade_direction
                    3. If height > size of screen, reset to 0
                    4. Increase speed
                    """
                    if sprite.x < screen_size[0] - sprite.w:
                        commands.move_right = True
                    else:
                        commands.jump_down = True
                        brain.invade_direction = 1
                else:
                    """
                    Algorithm:
                    1. Move left until hit edge of screen
                    2. Reduce height and change invade direction
                    3. If height > size of screen, reset to 0
                    4. Increase speed
                    """
                    if sprite.x > 0:
                        commands.move_left = True
                    else:
                        commands.jump_down = True
                        brain.invade_direction = 0

                if sprite.y + sprite.h == screen_size[1]:
                    sprite.y = 0


class AnimationSystem(ext.Applicator):
    """
    This system updates the sprite's source rect to point to the correct frame on it's
    spritesheet. The animation component contains a list of locations which are
    cycled through via the framecounter and index.
    """
    def __init__(self):
        super().__init__()
        print("Initializing Animation System.")
        self.componenttypes = Animation, Sprite, Velocity
        self.index = 0
        self.start_ticks = 0

    def process(self, world, componentsets):
        current_tick = SDL_GetTicks()
        start_ticks = self.start_ticks
        for animation, sprite, velocity in componentsets:
            # TODO: Optimize this. There may be wasted math here.
            if velocity.x is -velocity.max:
                sprite.src_rect = animation.far_left[animation.index]
            elif velocity.x < -0.1:
                sprite.src_rect = animation.mid_left[animation.index]
            elif velocity.x is velocity.max:
                sprite.src_rect = animation.far_right[animation.index]
            elif velocity.x > 0.1:
                sprite.src_rect = animation.mid_right[animation.index]
            else:
                sprite.src_rect = animation.idle[animation.index]
            animation.elapsed_ticks += current_tick - start_ticks
            if animation.elapsed_ticks > animation.frame_time:
                animation.elapsed_ticks = 0
                animation.index += 1
                if animation.index > len(animation.idle) - 1:
                    animation.index = 0
        self.start_ticks = current_tick


class CommandSystem(ext.Applicator):
    """
    Adjust Entitiy's velocity, and handle attacks, depending on their commands.
    """
    def __init__(self, renderer):
        super().__init__()
        print("Initializing Command System.")
        self.componenttypes = Commands, Velocity, SFX, Sprite
        self.renderer = renderer

    def _bullet_maker(self, world, image, posx, posy, angle, speed=8, enemy_fire=False, lifespan=60):
        bullet = Bullet(world=world,
                        sprite=sprite_maker.from_image(renderer=self.renderer, image_name=RESOURCES.get_path(image), size=(5, 10)),
                        posx=posx, posy=posy,
                        velx=(speed * cos(radians(angle - 90))),
                        vely=(speed * sin(radians(angle - 90))),
                        lifespan=lifespan)
        if enemy_fire:
            bullet.body.enemy = True

    def process(self, world, componentsets):
        for cmd, vel, sfx, spr in componentsets:
            # Move left or right, or decel if both or no buttons pressed.
            if cmd.move_left and cmd.move_right:
                vel.x *= vel.decel
            elif cmd.move_left:
                vel.x = max(vel.x - vel.accel, -vel.max)
            elif cmd.move_right:
                vel.x = min(vel.x + vel.accel, vel.max)
            else:
                vel.x *= vel.decel
            # Move up or down, or decel if both or no buttons pressed.
            if cmd.move_up and cmd.move_down:
                vel.y *= vel.decel
            elif cmd.move_up:
                vel.y = max(vel.y - vel.accel, -vel.max)
            elif cmd.move_down:
                vel.y = min(vel.y + vel.accel, vel.max)
            else:
                vel.y *= vel.decel
            # quickly burst towards a certain direction:
            if cmd.jump_up:
                vel.y = -vel.max * 2
            elif cmd.jump_down:
                vel.y = vel.max * 2
            elif cmd.jump_left:
                vel.x = -vel.max * 2
            elif cmd.jump_right:
                vel.x = vel.max * 2

            # Handle attack commands:
            if cmd.attack:
                sfx.play_me = sfx.attack
                self._bullet_maker(world=world, posx=spr.mid_x, posy=spr.mid_y, angle=spr.angle, image="blue.png")
            if cmd.enemy_attack:
                sfx.play_me = sfx.attack
                self._bullet_maker(world=world, posx=spr.mid_x, posy=spr.mid_y, angle=180, speed=4,
                                   enemy_fire=True, lifespan=100, image="red.png")

            # Reset all commands back to False:
            cmd.rotate_left = cmd.rotate_right = cmd.attack = cmd.enemy_attack = False
            cmd.move_left = cmd.move_right = cmd.move_up = cmd.move_down = False
            cmd.jump_up = cmd.jump_down = cmd.jump_left = cmd.jump_right = False


class MovementSystem(ext.Applicator):
    """
    Movement system. Simply adjusts the Entity's sprite position and angle based
    on it's current velocity. It also limits the sprites movements to within
    the screen border (min/maxx, min/maxy).
    """
    def __init__(self, minx, miny, maxx, maxy):
        super().__init__()
        print("Initializing Movement System.")
        self.componenttypes = Sprite, Velocity
        self.minx = minx
        self.miny = miny
        self.maxx = maxx
        self.maxy = maxy

    def process(self, world, componentsets):
        minx = self.minx
        miny = self.miny
        maxx = self.maxx
        maxy = self.maxy
        for sprite, vel in componentsets:
            # Adjust the sprite.x/y/angle by the velocity values.
            sprite.x += vel.x
            sprite.y += vel.y
            sprite.angle += vel.angular
            # If the sprite.x/y is outside of the screen, adjust it back.
            # (Hacked to allow bullets to go off screen but nothing bigger than 5 pixels).
            if sprite.size[0] > 5:
                sprite.x = max(minx, sprite.x)
                sprite_w, sprite_h = sprite.size
                pmaxx = sprite.x + sprite_w
                if pmaxx > maxx:
                    sprite.x = maxx - sprite_w
                sprite.y = max(miny, sprite.y)
                pmaxy = sprite.y + sprite_h
                if pmaxy > maxy:
                    sprite.y = maxy - sprite_h


class RenderSystem(ext.System):
    """
    A rendering system, created around the more complex SDL_RenderCopyEx,
    rather than SDL_RenderCopy. We can do rotation of sprites if we want.
    This will copy all sprites to the renderer in the correct position, and
    present (flip) the renderer when done.
    """
    def __init__(self, renderer):
        super().__init__()
        print("Initializing Rendering System.")
        self.componenttypes = (Sprite,)
        self.renderer = renderer

    def render(self, sprites):
        renderer = self.renderer
        renderer.clear()
        src_rect = SDL_Rect(0, 0, 0, 0)
        dst_rect = SDL_Rect(0, 0, 0, 0)
        if isiterable(sprites):
            for sp in sprites:
                src_rect.x, src_rect.y, src_rect.w, src_rect.h = sp.src_rect
                dst_rect.x = int(sp.x + -sp.zoom)
                dst_rect.y = int(sp.y + -sp.zoom)
                dst_rect.w = int(src_rect.w + sp.zoom * 2)
                dst_rect.h = int(src_rect.h + sp.zoom * 2)
                if SDL_RenderCopyEx(renderer.renderer, sp.texture, src_rect, dst_rect, sp.angle, None, SDL_FLIP_NONE) == -1:
                    raise SDL_Error()
        else:
            src_rect.x, src_rect.y, src_rect.w, src_rect.h = sprites.src_rect
            dst_rect.x = int(sprites.x + -sprites.zoom)
            dst_rect.y = int(sprites.y + -sprites.zoom)
            dst_rect.w = int(sprites.w + sprites.zoom)
            dst_rect.h = int(sprites.h + sprites.zoom)
            if SDL_RenderCopyEx(renderer.renderer, sprites.texture, src_rect, dst_rect, sprites.angle, None, SDL_FLIP_NONE) == -1:
                raise SDL_Error()
        renderer.present()

    def process(self, world, components):
        self.render(sorted(components, key=lambda spr: spr.depth))


class CollisionSystem(ext.Applicator):
    """
    A simple System for detecting rect overlaps among Entities. Some effort is made
    to separate entities into types, but it is basically doing brute force AABB checking.
    """
    def __init__(self, hud):
        super().__init__()
        print("Initializing Collision System")
        self.componenttypes = Sprite, Body
        self.player = []
        self.enemies = []
        self.bullets = []
        self.powerups = []
        self.hud = hud

    @staticmethod
    def _simple_overlap(first, second):
        """ A simple AABB check for overlaps. Takes two rects as input. """
        aleft, atop, aright, abottom = first
        bleft, btop, bright, bbottom = second
        # An overlap has occured if ALL of these are True, otherwise return False:
        return bleft < aright and bright > aleft and btop < abottom and bbottom > atop

    def process(self, world, componentsets):
        self.player.clear()
        self.enemies.clear()
        self.bullets.clear()
        for sprite, body in componentsets:
            if body.player:
                self.player.append((sprite, body))
            elif body.enemy:
                self.enemies.append((sprite, body))
            elif body.bullet:
                self.bullets.append((sprite, body))
            elif body.powerup:
                self.powerups.append((sprite, body))

        for player in self.player:
            for bullet in self.bullets:
                if bullet[1].enemy is True:
                    if self._simple_overlap(first=player[0].area, second=bullet[0].area):
                        player[1].has_collided = True
                        bullet[1].has_collided = True
            for enemy in self.enemies:
                if self._simple_overlap(first=player[0].area, second=enemy[0].area):
                    player[1].has_collided = True
                    enemy[1].has_collided = True
        for enemy in self.enemies:
            for bullet in self.bullets:
                if bullet[1].enemy is not True:
                    if self._simple_overlap(first=enemy[0].area, second=bullet[0].area):
                        enemy[1].has_collided = True
                        bullet[1].has_collided = True
                        self.hud.playerstats.current_score += 100


class LifeSystem(ext.Applicator):
    """
    This System handles dead Entities, and Entities with lifespans (particles, bullets).
    Dead Entities will be deleted, while those with finite lifespans will be counted down
    until dead. Explosions are also created when approprate.
    """
    def __init__(self, renderer):
        super().__init__()
        print("Initializing Life System.")
        self.componenttypes = Life, Body, Sprite
        self.renderer = renderer

    @staticmethod
    def _del_entity(world, life):
        for entity in world.entities:
            if entity.id == life.parent_id:
                world.delete(entity)
                break

    def _create_explosion(self, world, pos, lifespan):
        Explosion(world=world, sprite=sprite_maker.from_image(self.renderer, RESOURCES.get_path("explosion-1.png"),
                                                              (32, 32)), posx=pos[0], posy=pos[1], lifespan=lifespan)

    def process(self, world, componentsets):
        for life, body, sprite in componentsets:
            if life.lifespan:
                life.lifespan -= 1
                if life.lifespan <= 0:
                    life.is_alive = False
                    self._del_entity(world, life)

            if body.has_collided:
                if body.bullet:
                    self._del_entity(world, life)
                    continue             # Don't create explosions for bullet on bullet collisions.
                elif body.enemy:
                    self._create_explosion(world=world, pos=sprite.position, lifespan=30)
                    self._del_entity(world, life)
                elif body.player:
                    if life.extra_lives > 0:
                        continue
                    else:
                        life.is_alive = False


class ZoomSystem(ext.Applicator):
    """
    Checks the Sprite's zoom_target, and increases or decreases the zoom value
    by the zoom_speed, dending on whether or not the target is already met or not.
    Zooming is very simple: zoom values are in +/- pixels, NOT percentage.
    """
    def __init__(self):
        super().__init__()
        print("Initializing Zoom System.")
        self.componenttypes = Sprite, Zoom

    def process(self, world, componentsets):
        for spr, zoom in componentsets:
            if spr.zoom == zoom.zoom_target:
                continue                            # skip if zoom is already == target.
            elif spr.zoom < zoom.zoom_target:
                spr.zoom += zoom.zoom_speed
                if spr.zoom > 0:
                    spr.zoom = 0
            elif spr.zoom > zoom.zoom_target:
                spr.zoom -= zoom.zoom_speed
                if spr.zoom < 0:
                    spr.zoom = 0


class ParticleSystem(ext.Applicator):
    """
    Creates particles for any Entities that have an Emitter component.
    The particles are actually entities themselves that have a limited
    lifespan.
    """
    def __init__(self, renderer, world):
        super().__init__()
        print("Initializing Particle System")
        self.componenttypes = Emitter, Sprite
        self.renderer = renderer
        self.world = world

    def _create_particle(self, pos, color, size, max_life):
        # TODO: replace these calls to randint with a precalculated pseudorandom list.
        size = randint(size/2, size*2)
        color = color[0] + randint(-25, 25), color[1] + randint(-25, 25), color[2] + randint(-25, 25)
        Particle(world=self.world,
                 # sprite=sprite_maker.from_color(renderer=self.renderer, color=color, size=(size, size)),
                 sprite=sprite_maker.from_image(renderer=self.renderer, image_name=RESOURCES.get_path("red.png"), size=(size, size)),
                 pos=(pos[0] + randint(-size*2, size), pos[1] + randint(-size*2, size)),
                 lifespan=randint(max_life/5, max_life))

    def process(self, world, componentsets):
        create_particle = self._create_particle
        for emit, sprite in componentsets:
            create_particle(pos=sprite.mid_position, color=emit.color, size=emit.size, max_life=emit.max_life)


class EntityManagerSystem(ext.System):
    """
    A System for initializing the enemy waves. The waves are initialized one by one in order. When
    all waves are used, they are repeated from the beginning with a higher base Enemy speed.
    """
    def __init__(self, world, renderer, tiled_file):
        super().__init__()
        print("Initializing Entity Manager System")
        self.componenttypes = ext.Entity,
        self.world = world
        self.renderer = renderer
        self.living_things = []
        self.enemy_list = tiled_loader.get_layers(tiled_map=tiled_file)
        self.current_wave = 0
        self.level_scaling = 2
        self.ticker = 0
        self._initialize_enemy_wave()

    def _enemy_maker(self, ident, pos, pattern, sprite_sheet, size, speed, level_scaling):
        enemy = Enemy(world=self.world, sprite=sprite_maker.from_image(self.renderer, RESOURCES.get_path(sprite_sheet),
                      size=size), pattern=pattern, ident=ident, posx=pos[0], posy=pos[1], speed=speed)
        enemy.sprite.zoom = -16         # Add a "pop in" effect to enemies.
        enemy.zoom.zoom_speed = 0.2
        enemy.sfx.attack = "jfxr_enemy_shoot.wav"
        enemy.brain.intelligence = level_scaling
        self.living_things.append(enemy)

    def _initialize_enemy_wave(self):
        # Remove any old bullets/particles/explosions if present.
        old_effects_entities = []
        for entity in self.world.entities:
            if entity.__class__ in(Bullet, Particle, Explosion):
                old_effects_entities.append(entity)
        for entity in old_effects_entities:
            entity.delete()

        # Delete any leftover enemies, but track their IDs.
        remaining_enemy_ids = []
        for enemy in self.living_things:
            if enemy in self.world.entities:
                remaining_enemy_ids.append(enemy.ident.num)
                enemy.delete()

        if len(remaining_enemy_ids) > 0:
            for old_enemy_id in remaining_enemy_ids:
                for enemy in self.enemy_list[self.current_wave]["objects"]:
                    if old_enemy_id == enemy["id"]:
                        sprite_width = 32
                        sprite_height = 32
                        speed = self.level_scaling * 0.25
                        # TODO: Replace with function for looking up sprite size based upon type
                        if enemy["type"] == "invade":
                            sprite_width = 125
                            sprite_height = 238
                        if enemy["type"] == "ufo":
                            sprite_width = 32
                            sprite_height = 56
                        if enemy["type"] == "divebomb":
                            speed = 0.40 + self.level_scaling * 0.05

                        self._enemy_maker(pos=(enemy["x"], enemy["y"]), pattern=enemy["type"],
                                          ident=enemy["id"], sprite_sheet=enemy["name"],
                                          size=(sprite_width, sprite_height),
                                          speed=speed,
                                          level_scaling=self.level_scaling)
        else:
            if self.current_wave > len(self.enemy_list) - 1:
                self.current_wave = 0
                self.level_scaling += 1
            for enemy in self.enemy_list[self.current_wave]["objects"]:
                sprite_width = 32
                sprite_height = 32
                
                # TODO: Replace with function for looking up sprite size based upon type
                speed = self.level_scaling * 0.25
                if enemy["type"] == "invade":
                    sprite_width = 125
                    sprite_height = 238
                if enemy["type"] == "ufo":
                    sprite_width = 32
                    sprite_height = 56
                if enemy["type"] == "divebomb":
                    speed = 0.40 + self.level_scaling * 0.05
                self._enemy_maker(pos=(enemy["x"], enemy["y"]), pattern=enemy["type"],
                                  ident=enemy["id"], sprite_sheet=enemy["name"],
                                  size=(sprite_width, sprite_height), speed=speed,
                                  level_scaling=self.level_scaling)

    def process(self, world, components):
        # No need to check this every single frame, so use ticker.
        # (Using the ticker does mean that the living_things list will be slightly behind,
        # necessitating checking against the world's list on delete.
        if self.ticker >= 30:
            self.ticker = 0
            if len(self.living_things) is 0:
                # Advance wave (will trigger next level if no more waves in current level list)
                self.current_wave += 1
                self._initialize_enemy_wave()
            # TODO: Do this a different way so that the list doesn't need to be kept and parsed.
            # This will remove entities from the living_things list if
            # they have already been deleted from the world:
            for thing in self.living_things:
                if thing not in self.world.entities:
                    self.living_things.remove(thing)
        else:
            self.ticker += 1
