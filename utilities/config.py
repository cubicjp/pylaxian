import json


class Config(object):
    """
    Load the current configuration from disk. If the file does not exist, or is corrupted, start a new
    configuration file based on the default values in valid_keys.
    """
    def __init__(self, file_name):
        self.config_file_name = file_name
        valid_keys = {"fullscreen": False,
                      "rumble": True,
                      "music_vol": 50,
                      "sfx_vol": 50,
                      "highscore1": 0,
                      "highscore2": 0,
                      "highscore3": 0}

        try:
            json_file = open(file_name, "r")
            self.dat = json.load(json_file)
            json_file.close()
            print("Loading saved data...")
        except:
            print("Starting new save file...")
            self.dat = valid_keys
        if not all(key in self.dat for key in valid_keys):
            print("Save is corrupted... Resetting to default.")
            self.dat = valid_keys

    def save_data(self):
        """
        Save the current configuration details to disk. Make it pretty by using sorted_keys and indent.
        """
        with open(self.config_file_name, "w") as save_file:
            json.dump(self.dat, save_file, sort_keys=True, indent=1)

    def update_score(self, new_score):
        """
        Make a list of the current high scores plus the new score passed in. Save the top three, drop the lowest.
        """
        scores = [self.dat["highscore1"], self.dat["highscore2"], self.dat["highscore3"], new_score]
        self.dat["highscore1"], self.dat["highscore2"], self.dat["highscore3"] = sorted(scores, reverse=True)[:3]
