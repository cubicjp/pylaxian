from sdl2 import *


class Input:
    """
    Basic Input class that takes advantage of the new SDL_GameController API. Additional controller mappings
    are loaded from the resources/gamecontrollerdb.txt file if available. Haptic (Rumble) is also supported.
    """
    def init_haptic(self):
        if SDL_NumHaptics() > 0:
            self.haptic = SDL_HapticOpen(0)
            if SDL_HapticRumbleSupported(self.haptic) > 0:
                SDL_HapticRumbleInit(self.haptic)
                self.haptic_available = True
                print("Initialized haptic feedback")

    def __init__(self):
        # Initialize subsystem. Check if a joystick exists, AND is a GameController.
        # TODO: second controller support. Check for 1st when instantanced.
        SDL_Init(SDL_INIT_GAMECONTROLLER | SDL_INIT_HAPTIC)
        SDL_GameControllerAddMappingsFromFile("resources/gamecontrollerdb.txt".encode())
        self.haptic_available = False
        self.haptic = None
        if SDL_NumJoysticks() > 0:
            if SDL_IsGameController(0):
                self.controller = SDL_GameControllerOpen(0)
                self.controllername = SDL_GameControllerName(self.controller).decode()
                print("Initialized:", self.controllername)
        else:
            print("No game controller found.")

        self.current_inputs = {"up": 0, "down": 0, "left": 0, "right": 0, "a": 0, "b": 0, "x": 0, "y": 0,
                               "l": 0, "r": 0, "start": 0, "back": 0}
        self.prev_inputs = self.current_inputs.copy()

        self.button_a = SDL_CONTROLLER_BUTTON_A
        self.button_b = SDL_CONTROLLER_BUTTON_B
        self.button_x = SDL_CONTROLLER_BUTTON_X
        self.button_y = SDL_CONTROLLER_BUTTON_Y
        self.button_l = SDL_CONTROLLER_BUTTON_LEFTSHOULDER
        self.button_r = SDL_CONTROLLER_BUTTON_RIGHTSHOULDER
        self.button_back = SDL_CONTROLLER_BUTTON_BACK
        self.button_start = SDL_CONTROLLER_BUTTON_START
        self.dpad_up = SDL_CONTROLLER_BUTTON_DPAD_UP
        self.dpad_down = SDL_CONTROLLER_BUTTON_DPAD_DOWN
        self.dpad_left = SDL_CONTROLLER_BUTTON_DPAD_LEFT
        self.dpad_right = SDL_CONTROLLER_BUTTON_DPAD_RIGHT

    def rumble(self, intensity, length_ms):
        if self.haptic_available:
            SDL_HapticRumblePlay(self.haptic, intensity, length_ms)
            print(SDL_GetError().decode())
        else:
            pass

    # TODO: add remaping support for keyboard inputs.
    def update(self, new_events):

        self.prev_inputs = self.current_inputs.copy()

        for event in new_events:
            if event.type == SDL_KEYUP:
                if event.key.keysym.sym == SDLK_z:
                    self.current_inputs["a"] = 0
                if event.key.keysym.sym == SDLK_x:
                    self.current_inputs["b"] = 0
                if event.key.keysym.sym == SDLK_c:
                    self.current_inputs["x"] = 0
                if event.key.keysym.sym == SDLK_v:
                    self.current_inputs["y"] = 0
                if event.key.keysym.sym == SDLK_ESCAPE:
                    self.current_inputs["back"] = 0
                if event.key.keysym.sym == SDLK_RETURN:
                    self.current_inputs["start"] = 0
                if event.key.keysym.sym == SDLK_UP:
                    self.current_inputs["up"] = 0
                if event.key.keysym.sym == SDLK_DOWN:
                    self.current_inputs["down"] = 0
                if event.key.keysym.sym == SDLK_LEFT:
                    self.current_inputs["left"] = 0
                if event.key.keysym.sym == SDLK_RIGHT:
                    self.current_inputs["right"] = 0
            if event.type == SDL_KEYDOWN:
                if event.key.keysym.sym == SDLK_z:
                    self.current_inputs["a"] = 1
                if event.key.keysym.sym == SDLK_x:
                    self.current_inputs["b"] = 1
                if event.key.keysym.sym == SDLK_c:
                    self.current_inputs["x"] = 1
                if event.key.keysym.sym == SDLK_v:
                    self.current_inputs["y"] = 1
                if event.key.keysym.sym == SDLK_ESCAPE:
                    self.current_inputs["back"] = 1
                if event.key.keysym.sym == SDLK_RETURN:
                    self.current_inputs["start"] = 1
                if event.key.keysym.sym == SDLK_UP:
                    self.current_inputs["up"] = 1
                if event.key.keysym.sym == SDLK_DOWN:
                    self.current_inputs["down"] = 1
                if event.key.keysym.sym == SDLK_LEFT:
                    self.current_inputs["left"] = 1
                if event.key.keysym.sym == SDLK_RIGHT:
                    self.current_inputs["right"] = 1

            if event.type == SDL_CONTROLLERBUTTONUP:
                self.current_inputs["a"] = SDL_GameControllerGetButton(self.controller, self.button_a)
                self.current_inputs["b"] = SDL_GameControllerGetButton(self.controller, self.button_b)
                self.current_inputs["x"] = SDL_GameControllerGetButton(self.controller, self.button_x)
                self.current_inputs["y"] = SDL_GameControllerGetButton(self.controller, self.button_y)
                self.current_inputs["l"] = SDL_GameControllerGetButton(self.controller, self.button_l)
                self.current_inputs["r"] = SDL_GameControllerGetButton(self.controller, self.button_r)
                self.current_inputs["back"] = SDL_GameControllerGetButton(self.controller, self.button_back)
                self.current_inputs["start"] = SDL_GameControllerGetButton(self.controller, self.button_start)
                self.current_inputs["up"] = SDL_GameControllerGetButton(self.controller, self.dpad_up)
                self.current_inputs["down"] = SDL_GameControllerGetButton(self.controller, self.dpad_down)
                self.current_inputs["left"] = SDL_GameControllerGetButton(self.controller, self.dpad_left)
                self.current_inputs["right"] = SDL_GameControllerGetButton(self.controller, self.dpad_right)
            if event.type == SDL_CONTROLLERBUTTONDOWN:
                self.current_inputs["a"] = SDL_GameControllerGetButton(self.controller, self.button_a)
                self.current_inputs["b"] = SDL_GameControllerGetButton(self.controller, self.button_b)
                self.current_inputs["x"] = SDL_GameControllerGetButton(self.controller, self.button_x)
                self.current_inputs["y"] = SDL_GameControllerGetButton(self.controller, self.button_y)
                self.current_inputs["l"] = SDL_GameControllerGetButton(self.controller, self.button_l)
                self.current_inputs["r"] = SDL_GameControllerGetButton(self.controller, self.button_r)
                self.current_inputs["back"] = SDL_GameControllerGetButton(self.controller, self.button_back)
                self.current_inputs["start"] = SDL_GameControllerGetButton(self.controller, self.button_start)
                self.current_inputs["up"] = SDL_GameControllerGetButton(self.controller, self.dpad_up)
                self.current_inputs["down"] = SDL_GameControllerGetButton(self.controller, self.dpad_down)
                self.current_inputs["left"] = SDL_GameControllerGetButton(self.controller, self.dpad_left)
                self.current_inputs["right"] = SDL_GameControllerGetButton(self.controller, self.dpad_right)
