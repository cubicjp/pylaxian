from sdl2.sdlmixer import Mix_FadeInMusic


class Scene(object):
    def __init__(self):
        self.scene_name = None
        self.next_scene = None
        self.music = None

    def update_world(self):
        raise NotImplementedError


class SceneManager(object):
    """
    A very simple class for storing, updating, and switching
    between scene instances.
    """
    def __init__(self):
        self.scene_list = {}
        self.current_scene = None

    def add_scene(self, scene):
        """
        Adds a scene to the scene dictionary. Scenes should be
        class instances that inherit from the Scene class, and
        contain a scene_name string.
        :param scene:
        :return:
        """
        """
        :param scene:
        :return:
        """
        self.scene_list[scene.scene_name] = scene
        self.current_scene = scene

    def update(self):
        """
        Updates the current scene's world(). It also checks if there is a pending scene
        transition queued, and switches to it if there is.
        :return:
        """
        self.current_scene.update_world()
        if self.current_scene.next_scene:
            queued_scene = self.current_scene.next_scene
            self.current_scene.next_scene = None
            self.current_scene = self.scene_list[queued_scene]
            Mix_FadeInMusic(self.current_scene.music, -1, 1000)

