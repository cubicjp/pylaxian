from sdl2.ext import load_image
from sdl2 import SDL_CreateRGBSurface, SDL_MapRGB, SDL_FillRect, SDL_CreateTextureFromSurface, SDL_FreeSurface
from sdl2 import SDL_QueryTexture
from components import Sprite
from ctypes import c_int


def from_color(renderer, color, size, depth=0):
    """
    Create a texture on an arbitrary color and size.
    :param renderer: the current SDL renderer.
    :param color: a tuple of RGB values, 0-255.
    :param size: a tuple of width/height.
    :return: a sprite component.
    """
    r, g, b = color
    w, h = size
    surface = SDL_CreateRGBSurface(0, w, h, 32, 0, 0, 0, 0)
    rgb_map = SDL_MapRGB(surface.contents.format.contents, r, g, b)
    SDL_FillRect(surface.contents, None, rgb_map)
    texture = SDL_CreateTextureFromSurface(renderer.renderer, surface)
    SDL_FreeSurface(surface)
    del surface
    return Sprite(texture=texture, w=w, h=h, depth=depth)


def from_image(renderer, image_name, size=None, depth=0):
    """
    Create a texture from an image file. Full image path must be provided.
    :param renderer:
    :param image_name:
    :return:
    """
    surface = load_image(image_name)
    texture = SDL_CreateTextureFromSurface(renderer.renderer, surface)
    SDL_FreeSurface(surface)
    del surface
    if size:
        return Sprite(texture=texture, w=size[0], h=size[1])
    else:
        texture_width = c_int()
        texture_height = c_int()
        SDL_QueryTexture(texture, None, None, texture_width, texture_height)
        return Sprite(texture=texture, w=texture_width.value, h=texture_height.value, depth=depth)


def from_surface(renderer, surface, free_up=True, size=None, depth=0):
    """
    Create a texture from a provided surface. .
    :param renderer: The current SDL renderer.
    :param surface: The SDL Surface that the texture will be created from.
    :param free_up: The surface is deleted afterwords by default,
    but can be kept if desired by passing "free_up=False"
    :param size: The sprite size can be specified with a (w, h) tuple. Otherwise,
    the texture size is used.
    :return:
    """
    texture = SDL_CreateTextureFromSurface(renderer.renderer, surface)
    if free_up:
        SDL_FreeSurface(surface)
        del surface
    if size:
        return Sprite(texture=texture, w=size[0], h=size[1])
    else:
        texture_width = c_int()
        texture_height = c_int()
        SDL_QueryTexture(texture, None, None, texture_width, texture_height)
        return Sprite(texture=texture, w=texture_width.value, h=texture_height.value, depth=0)
