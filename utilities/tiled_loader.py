import json
from sdl2.ext import Resources

RESOURCES = Resources("resources")


def get_objects(tiled_map, layer_name):
    """
    :param tiled_map: The name of a json formatted tiled map (mapeditor.org)
    :param layer_name: The object layer name on the map containing the simple objects.
    :return: Returns a list of dictionaries, which have the following keys:
    'name', 'properties', 'rotation', 'y', 'height', 'width', 'visible', 'x', 'id', 'type'
    """
    with open(RESOURCES.get_path(tiled_map)) as tm:
        data = json.load(tm)
        # get a list of all the layer_data in a specific layer:
        layer_data = [layer for layer in data["layers"] if layer["name"] == layer_name]
        # return spawn objects as a list of dictionaries:
        return layer_data[0]["objects"]


def get_all_objects(tiled_map):
    """
    :param tiled_map: The name of a json formatted tiled map (mapeditor.org)
    :return: Returns a big list of objects for every object in every layer.
    """
    with open(RESOURCES.get_path(tiled_map)) as tm:
        data = json.load(tm)
        # get a list of all the layers:
        layers = [layer for layer in data["layers"]]
        objects = []
        for layer in layers:
            if layer["objects"]:
                objects.extend(layer["objects"])
        # return spawn objects as a list of dictionaries:
        return objects


def get_layers(tiled_map):
    """
    :param tiled_map:
    :return: Returns a list of object layers only. These layer names can be checked by their layer["name"] key.
    The layer's objects can be returned by their layer["objects"] key.
    """
    with open(RESOURCES.get_path(tiled_map)) as tm:
        data = json.load(tm)
        # return a list of all the layers, to be parsed later.
        return [layer for layer in data["layers"] if layer["objects"]]
